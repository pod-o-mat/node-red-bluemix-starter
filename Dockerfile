FROM ubuntu:18.04

WORKDIR /app

# pip3 install numpy pandas scikit-learn tensorflow
RUN apt-get update -y && \
    apt-get -y upgrade && \
    apt-get update -y && \
    apt-get install -y --no-install-recommends apt-utils && \
    apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates && \
    curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt -y install nodejs && \
    apt-get install -y build-essential \
                      python  python-dev python-pip \
                      python3 python3-dev python3-pip \
                      nodejs && \
    pip install --upgrade pip && \
    pip3 install --upgrade pip

COPY . /app
RUN pip3 install -r requirements.txt && \
    CODE=-1 ; while [ $CODE != 0 ] ; do npm install ; CODE=$? ; done && \
    CODE=-1 ; while [ $CODE != 0 ] ; do npm rebuild bcrypt --build-from-source ; CODE=$? ; done && \
    npm shrinkwrap

EXPOSE 1880
CMD npm start
