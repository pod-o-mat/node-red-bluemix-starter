/**
 * Copyright 2014, 2019 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

const fs   = require("fs");
const path = require("path");
const util = require("util");

const REGEX_LEADING_ALPHA = /^[^a-zA-Z]*/;
const REGEX_ALPHA_NUM = /[^a-zA-Z0-9]/g;

function _sanitizeAppName(name) {
    name = name || 'node-red';
    return name.toLowerCase().replace(REGEX_LEADING_ALPHA, '').replace(REGEX_ALPHA_NUM, '');
}

var userDir = path.join(__dirname,".node-red");
// Ensure userDir exists - something that is normally taken care of by
// localfilesystem storage when running locally
if(!fs.existsSync(userDir)) fs.mkdirSync(userDir);
if(!fs.existsSync(path.join(userDir,"node_modules"))) fs.mkdirSync(path.join(userDir,"node_modules"));

var settings = module.exports = {
    // -- hbouvier BEGIN

    // If you installed the optional node-red-dashboard you can set it's path
    // relative to httpRoot
    //ui: { path: "ui" },
    ui: { path: process.env.UI_PATH || 'dashboard' },
    couchDb: process.env.COUCHDB_DBNAME,
    // The following property can be used to configure cross-origin resource sharing
    // in the HTTP nodes.
    // See https://github.com/troygoode/node-cors#configuration-options for
    // details on its contents. The following is a basic permissive set of options:
    //httpNodeCors: {
    //    origin: "*",
    //    methods: "GET,PUT,POST,DELETE"
    //},
    httpNodeCors: JSON.parse(process.env.HTTP_NODE_CORS || '{}'),
    // Anything in this hash is globally available to all functions.
    // It is accessed as context.global.
    // eg:
    //    functionGlobalContext: { os:require('os') }
    // can be accessed in a function block as:
    //    context.global.os
    functionGlobalContext: { 
        // os:require('os'),
        // octalbonescript:require('octalbonescript'),
        // jfive:require("johnny-five"),
        // j5board:require("johnny-five").Board({repl:false})
        os: require('os'),
        crypto: require('crypto'),
        uuidgen: () => {
          return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            const r = Math.random() * 16 | 0x00;
            const v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
          });
        },
        env: (process.env.NODE_RED_ENV_STARTSWITH || "").split(',')
              .map(predicate => Object.keys(process.env)
                .filter(key => key.startsWith(predicate))
                .reduce((acc, key) => { acc[key] = process.env[key]; return acc; }, {})
              )
              .reduce((acc, obj) => Object.assign({}, acc, obj), {})
    },
    // -- hbouvier END

    uiPort: process.env.PORT || 1880,
    mqttReconnectTime: 15000,
    debugMaxLength: 1000,

    //Flag for enabling Appmetrics dashboard (https://github.com/RuntimeTools/appmetrics-dash)
    useAppmetrics: false,

    userDir: userDir,

    flowFile: "flows.json",

    // Add the bluemix-specific nodes in
    nodesDir: path.join(__dirname,"nodes"),

    // Blacklist the non-bluemix friendly nodes
    nodesExcludes:['90-exec.js','28-tail.js','10-file.js','23-watch.js'],

    // Enable module reinstalls on start-up; this ensures modules installed
    // post-deploy are restored after a restage
    autoInstallModules: true,

    // Move the admin UI
    httpAdminRoot: '/red',

    // Serve up the welcome page
    httpStatic: path.join(__dirname,"public"),

    functionGlobalContext: { },

    // Configure the logging output
    logging: {
        // Only console logging is currently supported
        console: {
            // Level of logging to be recorded. Options are:
            // fatal - only those errors which make the application unusable should be recorded
            // error - record errors which are deemed fatal for a particular request + fatal errors
            // warn - record problems which are non fatal + errors + fatal errors
            // info - record information about the general running of the application + warn + error + fatal errors
            // debug - record information which is more verbose than info + info + warn + error + fatal errors
            // trace - record very detailed logging + debug + info + warn + error + fatal errors
            // off - turn off all logging (doesn't affect metrics or audit)
            level: "info",
            // Whether or not to include metric events in the log output
            metrics: false,
            // Whether or not to include audit events in the log output
            audit: true
        },
        logstash: {
            level:'info',
            metrics:true,
            audit: true,
            handler: (conf) => {
                const mqtt = require('mqtt');
                const mqtt_url = process.env.MQTT_URL;
                const client = mqtt_url ? mqtt.connect(mqtt_url) : null;
                if (client) {
                    client.on('connect', (err) => {
                        util.log(`[info] logstash connected to '${mqtt_url}' :`, err)
                    });
                    client.on('error', err => {
                        util.log(`[error] logstash connecting to '${mqtt_url}'`, err);
                        console.log(err);
                        console.log("[error] logstash can't connect"+err);
                    });
                }

                // {"level":40,"msg":"Loading palette nodes","timestamp":1585968269044})
                // {"level":99,"nodeid":"689ab3ac.a3ba2c","event":"node.function.duration","msgid":"32b1d809.257f08","value":1.24,"timestamp":1585968276047}
                // {"level":40,"id":"9a2e4534.85b248","type":"mqtt-broker","msg":"Connected to broker: mqtt://mqtt:1883:1883","path":"global","name":"Mosquitto","timestamp":1585968276158}
                // {"event":"nodes.icons.get","level":98,"path":"/icons","ip":"192.168.128.1","timestamp":1585968276200}
                // {"event":"nodes.config.get","id":"node-red-contrib-iconvfile/iconvfile-out","level":98,"path":"/nodes/node-red-contrib-iconvfile/iconvfile-out","ip":"192.168.128.1","timestamp":1585968276595}
                // {"level":40,"msg":" - node-red-contrib-fs-ops:fs-ops-size","timestamp":1585968275316}
                // {"level":30,"msg":"\n\n---------------------------------------------------------------------\nYour flow credentials file is encrypted using a system-generated key.\n\nIf the system-generated key is lost for any reason, your credentials\nfile will not be recoverable, you will have to delete it and re-enter\nyour credentials.\n\nYou should set your own key using the 'credentialSecret' option in\nyour settings file. Node-RED will then re-encrypt your credentials\nfile using your chosen key the next time you deploy a change.\n---------------------------------------------------------------------\n","timestamp":1585968270573}
                // {"level":20,"id":"eba00f90.f47d2","type":"gql-sub","msg":"connection error: WebSocket was closed before the connection was established","path":"a4329038.2e7b3","z":"a4329038.2e7b3","name":"Subscription to Chat","timestamp":1585968275374}

                // Return the function that will do the actual logging
                return (message) => {
                    if (!client) return;
                    const {msg, timestamp, ...fields} = message;
                    const document = {
                        '@timestamp': (new Date(timestamp)).toISOString(),
                        '@tags': ['node-red'],
                        '@fields': fields,
                        message: msg
                    }
                    try {
                        client.publish('/elasticsearch/logstash', JSON.stringify(document));
                    } catch(err) { 
                        console.log(err);
                    }
                };
            }
        }

        // logstash: {
        //     level:'off',
        //     metrics:true,
        //     handler: function(conf) {
        //         var net = require('net');
        //         var logHost = 'logstash',logPort = 5000;
        //         var conn = new net.Socket();
        //         conn.connect(logPort,logHost)
        //             .on('connect',function() {
        //                 console.log("Logger connected")
        //             })
        //             .on('error', function(err) {
        //                 // Should attempt to reconnect in a real env
        //                 // This example just exits...
        //                 process.exit(1);
        //             });
        //         // Return the function that will do the actual logging
        //         return function(msg) {
        //             var message = {
        //                 '@tags': ['node-red', 'test'],
        //                 '@fields': msg,
        //                 '@timestamp': (new Date(msg.timestamp)).toISOString()
        //             }
        //             try {
        //                 conn.write(JSON.stringify(message)+"\n");
        //             }catch(err) { console.log(err);}
        //         }
        //     }
        // }
    }
};

// -- hbouvier BEGIN
if (process.env.GITHUB_CLIENT_ID && process.env.GITHUB_CLIENT_SECRET && process.env.GITHUB_CLIENT_ROLES) {
  settings.adminAuth = require('node-red-auth-github')({
      clientID: process.env.GITHUB_CLIENT_ID,
      clientSecret: process.env.GITHUB_CLIENT_SECRET,
      baseURL: process.env.GITHUB_CLIENT_BASEURL || "http://localhost:1880/",
      users: JSON.parse(process.env.GITHUB_CLIENT_ROLES)
  })
}
if (process.env.API_BASIC_AUTH_ROLES) {
  settings.httpNodeAuth = JSON.parse(process.env.API_BASIC_AUTH_ROLES);
}

// NODE_RED_STORAGE_NAME is automatically set by this applications manifest.
var storageServiceName = process.env.NODE_RED_STORAGE_NAME || new RegExp("^"+settings.couchAppname+".cloudantNoSQLDB");
var couchService = JSON.parse(process.env.COUCHDB_SERVICE || false);

if (!couchService) {
    util.log("Failed to find Cloudant service: "+storageServiceName);
    if (process.env.NODE_RED_STORAGE_NAME) {
        util.log(" - using NODE_RED_STORAGE_NAME environment variable: "+process.env.NODE_RED_STORAGE_NAME);
    }
    //fall back to localfilesystem storage
} else {
    util.log("Using Cloudant service: "+storageServiceName+" : "+settings.couchAppname);
    settings.storageModule = require("./couchstorage");
    settings.couchUrl = couchService.credentials.url;
}
/*
// -- hbouvier END


// Identify the Cloudant storage instance the application should be using.
var storageServiceName;
var storageServiceDetails;


if (process.env.NODE_RED_STORAGE_NAME) {
    // A service has been identifed by the NODE_RED_STORAGE_NAME env var.
    //  - check to see if the named service exists
    storageServiceDetails = appEnv.getService(process.env.NODE_RED_STORAGE_NAME);
    if (!storageServiceDetails) {
        util.log("Failed to find Cloudant service: "+process.env.NODE_RED_STORAGE_NAME+ " (NODE_RED_STORAGE_NAME)");
    } else {
        storageServiceName = process.env.NODE_RED_STORAGE_NAME
    }
} else {
    // No couch service specified by env var - look at the attached services
    var candidateServices = Object.values(appEnv.getServices()).filter(app => app.label === "cloudantNoSQLDB");
    if (candidateServices.length === 0) {
        util.log("No Cloudant service found");
    } else {
        // Use the first in the list - but warn if there are multiple incase we
        // are using the 'wrong' one.
        storageServiceName = candidateServices[0].name;
        storageServiceDetails = candidateServices[0];
        if (candidateServices.length > 1) {
            util.log("Multiple Cloudant services found - using "+storageServiceName+". Use NODE_RED_STORAGE_NAME env var to specify the required instance.");
        }
    }
}

if (!storageServiceName) {
    // No suitable service has been found. Fall back to localfilesystem storage
    util.log("Falling back to localfilesystem storage. Changes will *not* be saved across application restarts.");
} else {
    // Set the Cloudant storage module settings
    settings.cloudantService = {
        // The name of the service instance to use.
        name: storageServiceName,
        // The URL to use
        url: storageServiceDetails.credentials.url,
        // The name of the database to use
        db: process.env.NODE_RED_STORAGE_DB_NAME || _sanitizeAppName(appEnv.name),
        // The prefix for all document names stored by this instance.
        prefix: process.env.NODE_RED_STORAGE_APP_NAME || _sanitizeAppName(appEnv.name)
    }

    util.log("Using Cloudant service: "+storageServiceName+" db:"+settings.cloudantService.db+" prefix:"+settings.cloudantService.prefix);
    settings.storageModule = require("./cloudantStorage");
}

// -- hbouvier BEGIN
*/
// -- hbouvier END
